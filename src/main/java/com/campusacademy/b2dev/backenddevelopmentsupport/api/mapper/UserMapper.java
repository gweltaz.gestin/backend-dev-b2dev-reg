package com.campusacademy.b2dev.backenddevelopmentsupport.api.mapper;

import com.campusacademy.b2dev.backenddevelopmentsupport.api.model.UserDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.security.Principal;

@Mapper(componentModel = "spring")
public interface UserMapper {

    @Mapping(target = "username", source = "name")
    UserDTO map(Principal principal);
}
