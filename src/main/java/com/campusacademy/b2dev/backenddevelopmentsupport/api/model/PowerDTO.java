package com.campusacademy.b2dev.backenddevelopmentsupport.api.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PowerDTO extends PowerMinimalDTO {
    String description;
    int nbHero;

    public PowerDTO(Long id, String name, String description, int nbHero) {
        super(id, name);
        this.description = description;
        this.nbHero = nbHero;
    }
}
