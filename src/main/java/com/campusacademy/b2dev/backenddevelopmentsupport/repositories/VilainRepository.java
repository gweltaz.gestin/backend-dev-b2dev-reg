package com.campusacademy.b2dev.backenddevelopmentsupport.repositories;

import com.campusacademy.b2dev.backenddevelopmentsupport.model.Vilain;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VilainRepository extends JpaRepository<Vilain, Long> {

}
