package com.campusacademy.b2dev.backenddevelopmentsupport.repositories;

import com.campusacademy.b2dev.backenddevelopmentsupport.model.Power;
import com.campusacademy.b2dev.backenddevelopmentsupport.model.SuperHero;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface PowerRepository extends JpaRepository<Power, Long> {
}
